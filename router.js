const express = require('express');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

// eslint-disable-next-line new-cap
const router = express.Router();

const service = require('./service/storageService');

const swaggerOptions = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'User Rest API',
      version: '1.0.0',
      description: "Simple User management API.",
      contact: {
        name: "Les ambassadeurs"
      },
      servers: ["http://localhost:8001"]
    },
  },
  apis: ['router.js'], // files containing annotations as above
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
router.use('/api/v1/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

router.use('/', (req, res, next) => {
  if (req.url == '/') {
    res.redirect('/api/v1/storages');
  } else {
    next();
  }
});

/**
 * @openapi
 * /api/v1/storages:
 *   get:
 *     description: Returns all storage in database.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Returns (JSON) all users in database.
 *       404:
 *         description: Not Users in database.
 */
router.get('/api/v1/storages', service.getStorage);

/**
 * @openapi
 * paths:
 *   /api/v1/storage/{id}:
 *     get:
 *       description: Get storage by id.
 *       produces:
 *         - application/json
 *       parameters:
 *         - in: path
 *           name: id
 *           schema:
 *             type: string
 *           requires: true
 *           description: Storage id
 *       responses:
 *         200:
 *           description: Return (JSON) an storage by his id.
 *         404:
 *           description: Storage Not Found.
 */
router.get('/api/v1/storage/:id', service.getById);

/**
 * @openapi
 * paths:
 *   /api/v1/storage/{id}:
 *     delete:
 *       description: Delete storage by his id.
 *       produces:
 *         - application/json
 *       parameters:
 *         - in: path
 *           name: id
 *           schema:
 *             type: string
 *           requires: true
 *           description: User id
 *       responses:
 *         200:
 *           description: Return (JSON) an storage by his id.
 *         404:
 *           description: Storage Not Found.
 */
router.delete('/api/v1/storage/:id', service.delete);

/**
 * @openapi
 * /api/v1/storage:
 *   post:
 *     description: Add Storage in database.
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: Ajoute un donnée de stockage
 *             type: object
 *             required:
 *               - data
 *             properties:
 *               data:
 *                 type: object
 *     responses:
 *       200:
 *         description: Return (JSON) an storage by his id.
 *       501:
 *         description: Error message.
 */
router.post('/api/v1/storage', service.add);

router.use((req, res) => {
  res.status(404).send('Page non trouvée');
});

module.exports = router;