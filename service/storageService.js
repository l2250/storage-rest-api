const data = [];
let lastID = 0;

exports.getById = async (req,res) => {
    const id = req.params.id;
    let result = data.find(data => id === data.id);
    if (result) {
        return res.status(200).json(result);
    } else {
        return res.status(404).json('Storage not found');
    }
};

exports.getStorage = (req,res) => {
    return res.status(200).json(data);
};

exports.add = async (req, res) => {
    const addData = req.body.data;
    console.log(addData, lastID)
    lastID++;
    const insert  = {
        id: lastID,
        data: addData,
    }
    data.push(insert)
    return res.status(200).json(insert);
};

exports.delete = async (req, res, next) => {
    const id = req.params.id;
    const index = data.findIndex(obj => obj.id === id);
    if (index === -1) {
        data.splice(index,1)
        return res.status(200).json('delete OK');
    } else {
        return res.status(404).json('Storage not found');
    }

};